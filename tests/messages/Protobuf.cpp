#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <gtest/gtest.h>
#include <typeinfo>

#include "src/messages/Message.hpp"
#include "src/messages/Protobuf.hpp"
#include "src/networking/Peer.hpp"

namespace neuro {
namespace messages {
namespace test {

TEST(Protobuf, hello) {
  messages::Protobuf parser0;
  messages::Protobuf parser1;

  const auto header =
      std::make_shared<messages::Header>(1, 2, Buffer{"DEADC0DE"});
  Message msg_init(header);
  msg_init.insert(std::make_shared<messages::Hello>(1337));

  const auto proto_msg = parser0.serialize(msg_init);
  const auto msg_end = parser1.parse(*proto_msg);
  ASSERT_EQ(msg_init, *msg_end);
}

TEST(Protobuf, hello_world) {
  messages::Protobuf parser;

  const auto header =
      std::make_shared<messages::Header>(1, 2, Buffer{"DEADC0DE"});
  Message msg_init(header);
  msg_init.insert(std::make_shared<messages::Hello>());
  networking::Peers peers({std::make_shared<networking::Peer>("1.2.3.4", 1234),
                           std::make_shared<networking::Peer>("127.0.0.1", 4242)});
  msg_init.insert(std::make_shared<messages::World>(true, peers, 1664));

  const auto proto_msg = parser.serialize(msg_init);
  const auto msg_end = parser.parse(*proto_msg);
  ASSERT_EQ(msg_init, *msg_end);
}

} // namespace test
} // namespace messages
} // namespace neuro
