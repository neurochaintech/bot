#include "src/messages/Message.hpp"
#include <gtest/gtest.h>
#include <typeinfo>

namespace neuro {
namespace messages {
namespace test {

TEST(Message, Message) {
  Message m;
  ASSERT_EQ(m.header()->version(), 0); // need to be set later
  ASSERT_EQ(m.header()->id(), 0);
}

TEST(Message, Hello) {
  {
    Hello hello;
    ASSERT_EQ(hello.type, Type::HELLO);
  }
}

TEST(Message, World) {
  {
    World world;
    ASSERT_EQ(world.type, Type::WORLD);

    ASSERT_FALSE(world.accepted);
    ASSERT_EQ(world.peers.size(), 0);
    ASSERT_FALSE(world.request_id);
  }
}

} // namespace test
} // namespace messages
} // namespace neuro
