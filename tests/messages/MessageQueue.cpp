#include <gtest/gtest.h>
#include <typeinfo>

#include "src/messages/Message.hpp"
#include "src/messages/MessageQueue.hpp"
#include "logger.hpp"

namespace neuro {
namespace messages {
namespace test {

template <typename T, typename... U>
size_t function_address(std::function<T(U...)> f) {
  typedef T(fnType)(U...);
  fnType **fn_ptr = f.template target<fnType *>();
  return (size_t)*fn_ptr;
}

void callback_hello1(std::shared_ptr<const Header> header,
                     std::shared_ptr<const Body> body) {}

void callback_hello2(std::shared_ptr<const Header> header,
                     std::shared_ptr<const Body> body) {}

void callback_world1(std::shared_ptr<const Header> header,
                     std::shared_ptr<const Body> body) {}

class MessageQueueTest {
public:
  bool test_subscribe() {
    MessageQueue _mq;

    _mq.subscribe(messages::Type::HELLO, callback_hello1);
    _mq.subscribe(messages::Type::HELLO, callback_hello2);
    _mq.subscribe(messages::Type::WORLD, callback_world1);

    auto &hello_functions = _mq._callbacks[messages::Type::HELLO];
    auto &world_functions = _mq._callbacks[messages::Type::WORLD];
    auto hello1_address =
        function_address(MessageQueue::Callback(callback_hello1));
    bool found = false;
    for (auto &func : hello_functions) {
      if (function_address(func) == hello1_address) {
        found = true;
      }
    }
    if (!found)
      return false;

    auto hello2_address =
        function_address(MessageQueue::Callback(callback_hello2));
    found = false;
    for (auto &func : hello_functions) {
      if (function_address(func) == hello2_address) {
        found = true;
      }
    }
    if (!found)
      return false;

    auto world1_address =
        function_address(MessageQueue::Callback(callback_world1));
    found = false;
    for (auto &func : world_functions) {
      if (function_address(func) == world1_address) {
        found = true;
      }
    }

    return found;
  }

  bool test_pushing_message() {
    MessageQueue _mq;
    _mq.publish(std::make_shared<const messages::Message>());
    return !_mq._queue.empty();
  };

  bool test_message_broadcasting() {
    bool flag = false;
    MessageQueue _mq;
    _mq.run();
    _mq.subscribe(
        messages::Type::HELLO,
        [&flag](std::shared_ptr<const messages::Header>,
                std::shared_ptr<const messages::Body>) { flag = true; });
    auto message = std::make_shared<messages::Message>();
    message->insert(std::make_shared<messages::Body>(messages::Type::HELLO));
    _mq.publish(std::move(message));
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    return flag;
  }
};

TEST(MessageQueue, subscribe) {
  MessageQueueTest mqt;
  ASSERT_TRUE(mqt.test_subscribe());
}

TEST(MessageQueue, pushing_message) {
  MessageQueueTest mqt;
  ASSERT_TRUE(mqt.test_pushing_message());
}

TEST(MessageQueue, message_broadcasting) {
  MessageQueueTest mqt;
  ASSERT_TRUE(mqt.test_message_broadcasting());
}

} // namespace test
} // namespace messages
} // namespace neuro
