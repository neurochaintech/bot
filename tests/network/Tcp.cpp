#include "networking/tcp/Tcp.hpp"
#include "crypto/Ecc.hpp"
#include "messages/Message.hpp"
#include "messages/Protobuf.hpp"
#include "networking/Networking.hpp"
#include "networking/Peer.hpp"
#include "networking/TransportLayer.hpp"
#include <gtest/gtest.h>

namespace neuro {
namespace networking {

class Networking;

namespace test {

class Tcp {
public:
  Tcp() = default;
  bool test_connection() {
    networking::Networking network(7);
    auto parser = std::shared_ptr<messages::Protobuf>();
    crypto::Ecc keys1, keys2;
    Port port{31212}; // Maybe change this to the port to be used by the bot
    networking::Tcp tcp1(&network, &keys1, parser);
    networking::Tcp tcp2(&network, &keys2, parser);

    tcp1.accept(port);
    auto peer = std::make_shared<networking::Peer>("127.0.0.1", port);
    tcp2.connect(peer);

    tcp1._io_service.run_one();
    tcp2._io_service.run_one();

    return tcp1._connections.size() == 1 && tcp2._connections.size() == 1;
  }

  bool test_message_verification() {
    networking::Networking network(7);
    auto parser = std::shared_ptr<messages::Protobuf>();
    crypto::Ecc keys1, keys2;
    Port port{31212};
    networking::Tcp tcp1(&network, &keys1, parser);
    networking::Tcp tcp2(&network, &keys2, parser);
    auto mq = network.message_queue();

    bool verified = false;
    mq->subscribe(messages::Type::HELLO,
                 [&verified](std::shared_ptr<const messages::Header> header,
                             std::shared_ptr<const messages::Body> body) {
		    verified = header->verified();
                 });

    tcp1.accept(port);
    tcp2.connect("127.0.0.1", std::to_string(port));

    auto message = std::make_shared<messages::Message>();
    message->insert(std::make_shared<messages::Body>(messages::Type::HELLO));

    return verified;
  }
};

TEST(Tcp, ConnectionTest) {
  Tcp tcp_test;
  ASSERT_TRUE(tcp_test.test_connection());
}

} // namespace test
} // namespace networking
} // namespace neuro
