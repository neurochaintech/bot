#include <gtest/gtest.h>

#include "common_types.hpp"
#include "src/config/Bot.hpp"
#include "src/config/Peers.hpp"

namespace neuro {
namespace test {

TEST(Conf, load) {
  config::Bot bot ("./bot.json");

  ASSERT_EQ(bot.key_pub_path (), "test_keys.pub");
  ASSERT_EQ(bot.key_priv_path (), "test_keys.priv");

  const auto networking = bot.networking();
  ASSERT_EQ(networking.max_connections(), 42);

  const auto tcp = networking.tcp();
  ASSERT_TRUE(tcp);
  const std::vector<int> listen_ports_ref = {1337, 4242};
  ASSERT_EQ(listen_ports_ref, tcp->listen_ports());

  config::Peers peers_conf(tcp->peers_filepath());
  const auto peers = peers_conf.networking_peers();

  const auto key_pub_str = "308201333081EC06072A8648CE3D02013081E0020101302C06072A8648CE3D0101022100FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F3044042000000000000000000000000000000000000000000000000000000000000000000420000000000000000000000000000000000000000000000000000000000000000704410479BE667EF9DCBBAC55A06295CE870B07029BFCDB2DCE28D959F2815B16F81798483ADA7726A3C4655DA4FBFC0E1108A8FD17B448A68554199C47D08FFB10D4B8022100FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141020101034200040FC80CE528A15FDDA5597EA2ADD5B34C86122F5A704EDAAB48F815F0651E32CD22F119736508ED7B6210F395D0E5264BBAB73F010A0B906A91BC94CCD1C46EF2";
  const networking::Peer peer0_ref(networking::bai::make_address_v4("127.0.0.1"),
				   1337, Buffer(key_pub_str, Buffer::InputType::HEX));
  const networking::Peer peer1_ref(networking::bai::make_address_v4("127.0.0.1"),
				   1338);

  ASSERT_EQ(*peers[0], peer0_ref);
  ASSERT_EQ(*peers[1], peer1_ref);
}

}  // namespace test
}  // namespace neuro
