#include "Bot.hpp"
#include "logger.hpp"
#include <chrono>
#include <gtest/gtest.h>
#include <sstream>
#include <thread>

namespace neuro {
using namespace std::chrono_literals;

class Listener {
private:
  bool _received_connection0{false};
  bool _received_connection1{false};
  bool _received_hello{false};
  bool _received_world{false};

public:
  void handler_hello(std::shared_ptr<const messages::Header> header,
                     std::shared_ptr<const messages::Body> hello) {
    LOG_DEBUG << "It entered the handler_hello";
    _received_hello = true;
  }
  void handler_world(std::shared_ptr<const messages::Header> header,
                     std::shared_ptr<const messages::Body> hello) {

    LOG_DEBUG << "It entered the handler_world";
    _received_world = true;
  }
  void handler_connection0(std::shared_ptr<const messages::Header> header,
                           std::shared_ptr<const messages::Body> body) {

    LOG_DEBUG << "It entered the handler_connection0";
    _received_connection0 = true;
  }
  void handler_connection1(std::shared_ptr<const messages::Header> header,
                           std::shared_ptr<const messages::Body> body) {

    LOG_DEBUG << "It entered the handler_connection1";
    _received_connection1 = true;
  }

  ~Listener() {
    LOG_DEBUG << "Entered the destructor";
    EXPECT_TRUE(_received_connection0);
    EXPECT_TRUE(_received_connection1);
    EXPECT_TRUE(_received_hello);
    EXPECT_TRUE(_received_world);
  }
};

int main(int argc, char *argv[]) {
  std::stringstream conf0("{"
                          "    \"logs\": {"
                          "        \"severity\": \"debug\","
                          "        \"to_stdout\": true"
                          "    },"
                          "    \"networking\" : {"
                          "        \"max_connections\" : 3,"
                          "        \"tcp\" : {"
                          "            \"listen_ports\" : [ 1337 ]"
                          "        }"
                          "    }"
                          "}");
  std::stringstream peers0("{ \"peers\": []}");

  std::stringstream conf1("{"
                          "    \"logs\": {"
                          "        \"severity\": \"debug\","
                          "        \"to_stdout\": true"
                          "    },"
                          "    \"networking\" : {"
                          "        \"max_connections\" : 3,"
                          "        \"tcp\" : {"
                          "            \"listen_ports\" : [ 1338 ]"
                          "        }"
                          "    }"
                          "}");

  std::stringstream peers1("{ \"peers\": ["
                           "  {\"ip\" : \"127.0.0.1\", \"port\" : 1337},"
                           "  {\"ip\" : \"127.0.0.1\", \"port\" : 1339}"
                           "]}");

  std::stringstream conf2("{"
                          "    \"logs\": {"
                          "        \"severity\": \"debug\","
                          "        \"to_stdout\": true"
                          "    },"
                          "    \"networking\" : {"
                          "        \"max_connections\" : 3,"
                          "        \"tcp\" : {"
                          "            \"listen_ports\" : [ 1339 ]"
                          "        }"
                          "    }"
                          "}");

  std::stringstream peers2("{ \"peers\": ["
                           "  {\"ip\" : \"127.0.0.1\", \"port\" : 1337},"
                           "  {\"ip\" : \"127.0.0.1\", \"port\" : 1338}"
                           "]}");

  { Bot bot(conf2, peers2); }

  Listener listener;
  std::vector<std::shared_ptr<Bot>> bots;
  bots.emplace_back(std::make_shared<Bot>(conf0, peers0));
  bots.emplace_back(std::make_shared<Bot>(conf1, peers1));

  bots[1]->networking()->subscribe(
      messages::Type::HELLO,
      [&listener](std::shared_ptr<const messages::Header> header,
                  std::shared_ptr<const messages::Body> body) {
        listener.handler_hello(header, body);
      });
  bots[0]->networking()->subscribe(
      messages::Type::WORLD,
      [&listener](std::shared_ptr<const messages::Header> header,
                  std::shared_ptr<const messages::Body> body) {
        listener.handler_world(header, body);
      });
  bots[0]->networking()->subscribe(
      messages::Type::CONNECTION,
      [&listener](std::shared_ptr<const messages::Header> header,
                  std::shared_ptr<const messages::Body> body) {
        listener.handler_connection0(header, body);
      });
  bots[1]->networking()->subscribe(
      messages::Type::CONNECTION,
      [&listener](std::shared_ptr<const messages::Header> header,
                  std::shared_ptr<const messages::Body> body) {
        listener.handler_connection1(header, body);
      });

  bots[0]->keep_max_connections();
  bots[1]->keep_max_connections();

  std::this_thread::sleep_for(1s);
  auto message = std::make_shared<messages::Message>();
  message->insert(std::make_shared<messages::Hello>(1337));
  bots[0]->networking()->send(message);

  std::this_thread::sleep_for(1s);

  return 0;
}

} // namespace neuro

int main(int argc, char *argv[]) { return neuro::main(argc, argv); }
