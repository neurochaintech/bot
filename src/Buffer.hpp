#ifndef NEURO_SRC_BUFFER_HPP
#define NEURO_SRC_BUFFER_HPP

#include <fstream>
#include <iostream>
#include <string>

#include "common_types.hpp"

namespace neuro {

class Buffer : public std::vector<uint8_t> {
 public:
  enum class InputType { HEX };

 private:
  inline uint8_t char2uint(const char c) {
    if (c >= '0' && c <= '9') {
      return c - '0';
    }
    if (c >= 'A' && c <= 'F') {
      return c - 'A' + 10;
    }
    if (c >= 'a' && c <= 'f') {
      return c - 'a' + 10;
    }

    throw std::runtime_error("Bad input");
  }

 public:
  Buffer() = default;
  Buffer(const Buffer &) = default;
  Buffer(Buffer &&) = default;
  Buffer(size_type count, uint8_t v) : std::vector<uint8_t>(count, v) {}
  Buffer(const std::string &string)
      : std::vector<uint8_t>(string.cbegin(), string.cend()) {}

  Buffer(const std::initializer_list<uint8_t> init) : vector<uint8_t>(init) {}

  Buffer(const std::string &string, const InputType input_type) {
    if ((string.size() % 2) == 1) {
      throw std::runtime_error("Bad input size");
    }

    switch (input_type) {
      case InputType::HEX: {
        auto it = string.begin(), end = string.end();
        while (it < end) {
          push_back((char2uint(*it) << 4) + char2uint(*(it + 1)));
          it += 2;
        }
      } break;
      default:
        throw std::runtime_error("Unknown type");
    }
  }

  void save(const std::string &filepath) {
    std::ofstream of(filepath, std::ios::binary);
    of.write(reinterpret_cast<const char *>(data()), size());
  }

  void copy(const uint8_t *data, const std::size_t size) {
    resize(size);
    std::copy(data, data + size, begin());
  }

  void copy(const std::string &string) {
    resize(string.size());
    std::copy(string.begin(), string.end(), begin());
  }

  void copy(const Buffer &buffer) {
    resize(buffer.size());
    std::copy(buffer.begin(), buffer.end(), begin());
  }
  
  bool operator==(const Buffer &other) {
    if (other.size() != this->size()) {
      return false;
    }
    size_t inner_size = this->size();
    for (size_t i = 0; i < inner_size; i += 1) {
      if (*(this->data()+i) != other[i]) {
        return false;
      }
    }

    return true;
  }
};

std::ostream &operator<<(std::ostream &os, const Buffer &buffer);

}  // namespace neuro

#endif /* NEURO_SRC_BUFFER_HPP */
