#include "config/Networking.hpp"
#include "config/Parser.hpp"

namespace neuro {
namespace config {

void Networking::load(std::shared_ptr<Parser> parser) {
  _max_connections = parser->get("max_connections", 7);

  std::string status_str = parser->get("keep_status", "");
  if (status_str == "CONNECTED") {
    _keep_status = networking::Peer::Status::CONNECTED;
  } else if (status_str == "REACHABLE") {
    _keep_status = networking::Peer::Status::REACHABLE;
  } else if (status_str == "ALL") {
    _keep_status = networking::Peer::Status::FULL;
  } else {
    LOG_INFO << "Unknown keep_status - Using default option: CONNECTED";
    _keep_status = networking::Peer::Status::CONNECTED;
  }

  auto tcp_opt = parser->sub_parser("tcp");
  if (tcp_opt) {
    _tcp = std::make_optional<Tcp>();
    _tcp->load(tcp_opt);
  }
}

void Networking::save(Parser *parser) {}

std::size_t Networking::max_connections() const { return _max_connections; }
const std::optional<Tcp> Networking::tcp() const { return _tcp; }

const networking::Peer::Status Networking::keep_status() const {
  return _keep_status;
}

} // namespace config
} // namespace neuro
