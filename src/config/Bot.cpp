#include "Bot.hpp"
#include "crypto/EccPub.hpp"
#include "logger.hpp"
#include "networking/Peer.hpp"

namespace neuro {
namespace config {

Bot::Bot(std::istream &json_istream) {
  auto parser = std::make_shared<JSONParser>(json_istream);
  load(parser);
}

Bot::Bot(const std::string &filepath) {
  auto parser = std::make_shared<JSONParser>(filepath);
  load(parser);
}

void Bot::load(std::shared_ptr<Parser> parser) {
  _key_pub_path = parser->get("key_pub", "");
  _key_priv_path = parser->get("key_priv", "");
  if (_key_pub_path.empty() || _key_priv_path.empty()) {
    LOG_WARNING << "Missing keys in configuration";
  }

  _networking.load(parser->sub_parser("networking"));
  _logs.load(parser->sub_parser("logs"));
}

const std::string Bot::key_pub_path() const { return _key_pub_path; }

const std::string Bot::key_priv_path() const { return _key_priv_path; }

const Networking &Bot::networking() const { return _networking; }

Networking *Bot::mutable_networking() { return &_networking; }

const Logs &Bot::logs() const { return _logs; }

void Bot::save(Parser *parser) {}

} // namespace config
} // namespace neuro
