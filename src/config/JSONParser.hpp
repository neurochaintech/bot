#ifndef NEURO_SRC_CONFIG_JSONPARSER_HPP
#define NEURO_SRC_CONFIG_JSONPARSER_HPP

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <istream>
#include <vector>

#include "config/Parser.hpp"

namespace neuro {
namespace config {

class JSONParser : public Parser {
private:
  boost::property_tree::ptree _config;

  void load(std::istream &json_istream);

  template <class T>
  T _get(const std::string &tag, T default_value) const {
    return _config.get<T>(tag, default_value);
  }
  
  template <class T>
  void _as_vector(const std::string &tag, std::vector<T> *v) const {
    for (const auto &item : _config.get_child(tag)) {
      v->emplace_back(item.second.get_value<T>());
    }
  }

 public:

  JSONParser(std::istream &json_stream);
  JSONParser(const std::string &filepath);
  JSONParser(const Parser &parser);
  JSONParser(const JSONParser &) = default;
  JSONParser(const boost::property_tree::ptree &config);

  std::shared_ptr<Parser> sub_parser(const std::string &tag) const;
  
  std::string get(const std::string &tag,
                  const std::string &default_value) const {
    return _get<std::string>(tag, default_value);
  }

  int get(const std::string &tag, const int default_value) const {
    return _get(tag, default_value);
  }

  std::size_t get(const std::string &tag,
                  const std::size_t default_value) const {
    return _get(tag, default_value);
  }

  float get(const std::string &tag, const float default_value) const {
    return _get(tag, default_value);
  }


  void as_vector(const std::string &tag, std::vector<std::string> *v) const {
    _as_vector(tag, v);
  }
  void as_vector(const std::string &tag, std::vector<int> *v) const {
    _as_vector(tag, v);
  }
  void as_vector(const std::string &tag, std::vector<std::size_t> *v) const {
    _as_vector(tag, v);
  }
  void as_vector(const std::string &tag, std::vector<float> *v) const {
    _as_vector(tag, v);
  }

  void as_vector(const std::string &tag, std::vector<std::shared_ptr<Parser>> *v) const {
    for (const auto &child : _config.get_child(tag)){
      v->emplace_back(std::make_shared<JSONParser>(child.second));
    }
  }
  // std::vector<std::string> get_children_list(const std::string &from_tag) const;
  

};

}  // namespace config
}  // namespace neuro

#endif
