#ifndef NEURO_SRC_CONFIG_PARSER_HPP
#define NEURO_SRC_CONFIG_PARSER_HPP

#include <string>
#include <vector>
#include <memory>

namespace neuro {
namespace config {

class Parser {
 public:
  class iterator;
  virtual std::string get(const std::string &tag,
                          const std::string &default_value) const = 0;
  virtual int get(const std::string &tag, const int default_value) const = 0;
  virtual std::size_t get(const std::string &tag,
			  const std::size_t default_value) const = 0;
  virtual float get(const std::string &tag,
                    const float default_value) const = 0;

  virtual std::shared_ptr<Parser> sub_parser(const std::string &tag) const  = 0;

  virtual void as_vector(const std::string &tag, std::vector<std::string> *v) const = 0;
  virtual void as_vector(const std::string &tag, std::vector<int> *v) const = 0;
  virtual void as_vector(const std::string &tag, std::vector<std::size_t> *v) const = 0;
  virtual void as_vector(const std::string &tag, std::vector<float> *v) const = 0;
  virtual void as_vector(const std::string &tag, std::vector<std::shared_ptr<Parser>> *v) const = 0;
  virtual ~Parser(){}
};

}  // namespace config
}  // namespace neuro

#endif /* NEURO_SRC_CONFIG_PARSER_HPP */
