#include "Logs.hpp"

namespace neuro {
namespace config {

void Logs::load(std::shared_ptr<Parser> parser) {
  _file_path = parser->get("file_path", "");
  _severity = parser->get("severity", "");
  _stdout = parser->get("to_stdout", true);
  _rotation_size_mb = parser->get("rotation_file_size_MB", 5);
}

void Logs::save(Parser *parser) {}

const std::string &Logs::file_path() const { return _file_path; }

const std::string &Logs::severity() const { return _severity; }

bool Logs::stdout() const { return _stdout; }

const size_t Logs::rotation_size_mb() const { return _rotation_size_mb; }

} // namespace config
} // namespace neuro
