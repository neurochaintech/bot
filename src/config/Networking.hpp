#ifndef NEURO_SRC_CONFIG_NETWORKING_HPP
#define NEURO_SRC_CONFIG_NETWORKING_HPP

#include <cstddef>
#include "config/Conf.hpp"
#include "config/Tcp.hpp"
#include "networking/Peer.hpp"

namespace neuro {
namespace config {

class Networking : public Conf {
 private:
  std::size_t _max_connections{0};
  std::optional<Tcp> _tcp;
  networking::Peer::Status _keep_status;

 public:
  void load(std::shared_ptr<Parser> parser);
  void save(Parser *parser);

  std::size_t max_connections() const;
  const std::optional<Tcp> tcp() const;
  const networking::Peer::Status keep_status() const;
  ~Networking(){}
};

}  // namespace config
}  // namespace neuro

#endif /* NEURO_SRC_CONFIG_NETWORKING_HPP */


