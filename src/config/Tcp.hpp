#ifndef NEURO_SRC_CONFIG_TCP_HPP
#define NEURO_SRC_CONFIG_TCP_HPP

#include <vector>
#include "common_types.hpp"
#include "config/Conf.hpp"
#include "config/Peers.hpp"

namespace neuro {
namespace config {

class Tcp : public Conf {
 public:

 private:
  std::vector<int> _listen_ports;
  std::string _parser;
  std::string _peers_filepath;

 public:
  void load(std::shared_ptr<Parser> parser);
  void save(Parser *parser);

  std::vector<int> listen_ports() const;

  const std::string &peers_filepath() const;
  const std::string &parser() const;
};

}  // config
}  // neuro

#endif /* NEURO_SRC_CONFIG_TCP_HPP */
