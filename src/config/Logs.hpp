#ifndef NEURO_SRC_CONFIG_LOGS_HPP
#define NEURO_SRC_CONFIG_LOGS_HPP

#include "config/Conf.hpp"

namespace neuro {
namespace config {

class Logs : Conf {
private:
  std::string _file_path;
  std::string _severity;
  bool _stdout;
  size_t _rotation_size_mb;

public:
  void load(std::shared_ptr<Parser> parser);
  void save(Parser *parser);

  const std::string& file_path() const;
  const std::string& severity() const;
  bool stdout() const;
  const size_t rotation_size_mb() const;
};

} // namespace config
} // namespace neuro

#endif
