#ifndef NEURO_SRC_COMMONS_CONFIG_BOTCONFIG_HPP
#define NEURO_SRC_COMMONS_CONFIG_BOTCONFIG_HPP

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <istream>
#include <vector>

#include "config/Conf.hpp"
#include "config/JSONParser.hpp"
#include "config/Logs.hpp"
#include "config/Networking.hpp"
#include "crypto/Ecc.hpp"
#include "networking/Peer.hpp"

#include "common_types.hpp"
namespace neuro {
namespace config {

class Bot : public Conf {
private:
  Logs _logs;
  std::string _key_pub_path;
  std::string _key_priv_path;
  Networking _networking;

public:
  Bot(std::istream &json_istream);
  Bot(const std::string &filepath);
  void save(Parser *parser);
  void load(std::shared_ptr<Parser>);

  const std::string key_pub_path() const;
  const std::string key_priv_path() const;
  const Networking &networking() const;
  Networking *mutable_networking();
  const Logs &logs() const;
};

} // namespace config
} // namespace neuro

#endif
