#include "config/Peers.hpp"
#include "config/JSONParser.hpp"
#include "networking/Peer.hpp"
#include <memory.h>

namespace neuro {
namespace config {

Peers::Peers(const std::string &filepath) {
  auto parser = std::make_shared<JSONParser>(filepath);
  _filename = filepath;
  load(parser);
}

Peers::Peers(std::istream &json_istream) {
  auto parser = std::make_shared<JSONParser>(json_istream);
  load(parser);
}

void Peers::load(std::shared_ptr<Parser> parser) {
  std::vector<std::shared_ptr<Parser>> peers;
  parser->as_vector("peers", &peers);
  for (const auto &peer : peers) {
    const auto ip_str = peer->get("ip", "");
    if (ip_str.empty()) {
      LOG_ERROR << "Missing ip for peer";
      continue;
    }

    const auto ip = networking::bai::make_address(ip_str);
    const auto port = peer->get("port", 0);
    if (port == 0) {
      LOG_ERROR << "Missing port for peer";
    }

    const auto key_pub_str = peer->get("key_pub", "");
    if (key_pub_str.empty()) {
      _peers.emplace_back(std::make_shared<networking::Peer>(ip, port));
    } else {
      _peers.emplace_back(std::make_shared<networking::Peer>(
          ip, port,
          crypto::EccPub(Buffer{key_pub_str, Buffer::InputType::HEX})));
    }
  }
}

bool Peers::write(const networking::Peers &peers, const std::string &filename) {
  using boost::property_tree::ptree;
  ptree out_node, node_list;
  std::vector<ptree> peers_nodes;
  for (const auto &peer : peers) {
    ptree peer_node;
    peer_node.put("ip", peer->ip().to_string());
    peer_node.put("port", peer->port());
    if (peer->key_pub()) {
      std::stringstream ss;
      ss << *peer->key_pub();
      peer_node.put("key_pub", ss.str());
    }
    peers_nodes.push_back(peer_node);
  }
  for (const auto &node : peers_nodes) {
    node_list.push_back(std::make_pair("", node));
  }
  out_node.push_back(std::make_pair("peers", node_list));
  const auto fn = filename.empty() ? _filename : filename;
  if (fn.empty()) {
    boost::property_tree::json_parser::write_json(std::cout, out_node);
  } else {
    std::ofstream fs(fn);
    if (!fs.is_open()) {
      LOG_ERROR << "Could not open " << fn << " to write the peers list";
      return false;
    }
    boost::property_tree::json_parser::write_json(fs, out_node);
    fs.close();
    LOG_INFO << "Peers file " << fn << " updated";
  }
  return true;
}

const networking::Peers &Peers::networking_peers() const { return _peers; }
void Peers::save(Parser *parser) {}

} // namespace config
} // namespace neuro
