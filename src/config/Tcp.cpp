#include "config/Tcp.hpp"

namespace neuro {
namespace config {

void Tcp::load(std::shared_ptr<Parser> parser) {
  parser->as_vector("listen_ports", &_listen_ports);
  _parser = parser->get("parser", "protobuf");
  _peers_filepath = parser->get("peers", "");
}

void Tcp::save(Parser *parser) {}

std::vector<int> Tcp::listen_ports() const { return _listen_ports; }

const std::string &Tcp::peers_filepath() const { return _peers_filepath; }

const std::string &Tcp::parser() const { return _parser; }

}  // namespace config
}  // namespace neuro
