#ifndef NEURO_SRC_CONF_CONF_HPP
#define NEURO_SRC_CONF_CONF_HPP

#include "config/Parser.hpp"

namespace neuro {
namespace config {

class Conf {
public:
  virtual void load(std::shared_ptr<Parser> parser) = 0;
  virtual void save(Parser *parser) = 0;
  virtual ~Conf(){}
};

}  // config
}  // neuro

#endif /* NEURO_SRC_CONF_CONF_HPP */
