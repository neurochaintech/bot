#ifndef NEURO_SRC_CONFIG_PEERS_HPP
#define NEURO_SRC_CONFIG_PEERS_HPP

#include "config/Conf.hpp"
#include "logger.hpp"
#include "networking/Peer.hpp"

namespace neuro {
namespace config {


class Peers : public Conf {
 private:
  networking::Peers _peers;
  std::string _filename;

 public:
  Peers(const std::string &filepath);
  Peers(std::istream &json_istream);
  void load(std::shared_ptr<Parser> parser);
  void save(Parser *parser);

  bool write(const networking::Peers &peers, const std::string &filename="");

  const networking::Peers &networking_peers() const;
};

}  // namespace config
}  // namespace neuro

#endif /* NEURO_SRC_CONFIG_PEERS_HPP */
