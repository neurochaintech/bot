#include "JSONParser.hpp"
#include "logger.hpp"

#include <iostream>
#include <iterator>

namespace neuro {
namespace config {

void JSONParser::load(std::istream &json_istream) {
  try {
    boost::property_tree::read_json(json_istream, _config);
  } catch (...) {
    LOG_ERROR << json_istream.rdbuf();
    throw std::runtime_error(
        "Error loading the configuration file. "
        "Check the JSON structure in the file");
  }
}

JSONParser::JSONParser(std::istream &json_istream) { load(json_istream); }

JSONParser::JSONParser(const std::string &filepath) {
  std::ifstream fs(filepath);
  if (!fs.is_open()) {
    std::string s = "Error loading the configuration file. (" + filepath + ")";
    throw std::runtime_error(s);
  }
  load(fs);
  fs.close();
}

JSONParser::JSONParser(const boost::property_tree::ptree &config)
    : _config(config) {}

std::shared_ptr<Parser> JSONParser::sub_parser(const std::string &tag) const {
  auto child_opt = _config.get_child_optional(tag);
  if (child_opt) {
    return std::make_shared<JSONParser>(*child_opt);
  } else {
    return {};
  }
}

}  // namespace config
}  // namespace neuro
