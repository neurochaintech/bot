set(ProtoFiles "${CMAKE_CURRENT_SOURCE_DIR}/networking/proto/Messages.proto")
PROTOBUF_GENERATE_CPP(ProtoSources ProtoHeaders ${ProtoFiles})

add_library(protos
  ${ProtoSources}
  ${ProtoHeaders}
  )

target_link_libraries(protos
  protobuf::libprotobuf
  )

add_library(commons
  Buffer.hpp
  Buffer.cpp
  common_types.hpp
  config/Bot.cpp
  config/Bot.hpp
  config/Conf.hpp
  config/Logs.hpp
  config/Logs.cpp
  config/JSONParser.cpp
  config/JSONParser.hpp
  config/Networking.cpp
  config/Networking.hpp
  config/Parser.hpp
  config/Peers.cpp
  config/Peers.hpp
  config/Tcp.cpp
  config/Tcp.hpp
  logger.hpp
  logger.cpp
  Bot.cpp
  Bot.hpp
)

target_link_libraries(commons
  Boost::system
  cryptopp-static
  Boost::filesystem
  Boost::thread
  Boost::log
  )


add_library(networking
  networking/Connection.hpp
  networking/Networking.hpp
  networking/Networking.cpp
  networking/Peer.hpp
  networking/Peer.cpp
  networking/tcp/Connection.hpp
  networking/tcp/Connection.cpp
  networking/tcp/Tcp.hpp
  networking/tcp/Tcp.cpp
  networking/TransportLayer.hpp
  networking/TransportLayer.cpp
  common_types.hpp
  )


target_link_libraries(networking
  commons
  Boost::system
  pthread
  cryptopp-static
  protos
  Boost::filesystem
  Boost::thread
  Boost::log
  )

add_library(messages
  messages/Message.hpp
  messages/Message.cpp
  messages/MessageQueue.hpp
  messages/MessageQueue.cpp
  messages/Parser.hpp
  messages/ParserFactory.cpp
  messages/Protobuf.cpp
  messages/Protobuf.hpp
  )

target_link_libraries(messages
  protos
  commons
  networking
  cryptopp-static
  Boost::system
  Boost::filesystem
  Boost::thread
  Boost::log
  )

add_library(cryptocc
  crypto/Ecc.cpp
  crypto/Ecc.hpp
  crypto/EccPriv.cpp
  crypto/EccPriv.hpp
  crypto/EccPub.hpp
  crypto/EccPub.cpp
  crypto/Hash.cpp
  crypto/Hash.hpp
  common_types.hpp
  )

target_link_libraries(cryptocc
  commons
  cryptopp-static
  Boost::system
  Boost::filesystem
  Boost::thread
  Boost::log
  )

add_executable(bot
  main.cpp
  )

target_link_libraries(bot
  commons
  networking
  cryptocc
  messages
  cryptopp-static
  Boost::program_options
  Boost::system
  Boost::filesystem
  Boost::thread
  Boost::log
  )


include_directories(${CMAKE_SOURCE_DIR}/src ${CMAKE_BINARY_DIR}/src)
#get_cmake_property(_variableNames VARIABLES)
#list (SORT _variableNames)
#foreach (_variableName ${_variableNames})
#    message(STATUS "var>${_variableName}=${${_variableName}}")
#endforeach()
