#include "messages/Message.hpp"
#include "messages/Protobuf.hpp"

namespace neuro {
namespace messages {

std::ostream &operator<<(std::ostream &os, const Message &message) {
  Protobuf protobuf;
  os << "msg> " << protobuf.to_json(message);

  return os;
}

std::shared_ptr<const Body> Message::body(const Type type) {
  for (const auto &body : _bodys) {
    if (body->type == type) {
      return body;
    }
  }
  return {};
}

bool operator==(const Body &a, const Body &b) {
  if (a.type != b.type) {
    return false;
  }

  switch (a.type) {
  case Type::HELLO: {
    const auto da = static_cast<const Hello *>(&a);
    const auto db = static_cast<const Hello *>(&b);

    return (*da == *db);
  }
  case Type::WORLD: {
    const auto da = static_cast<const World *>(&a);
    const auto db = static_cast<const World *>(&b);

    return (*da == *db);
  }
  default:
    return false;
  }

  return false;
}

bool operator==(const Header &a, const Header &b) {
  return (a.version() == b.version() && a.id() == b.id() &&
          a.verified() == b.verified() && a.sender_key() == b.sender_key() &&
          a.transport_layer_id() == b.transport_layer_id() &&
          a.connection_id() == b.connection_id());
}

bool operator==(const Hello &a, const Hello &b) {
  return (a.listen_port == b.listen_port);
}

bool operator==(const World &a, const World &b) {
  return (a.accepted == b.accepted && a.peers == b.peers && a.request_id &&
          b.request_id);
}

bool operator==(const Message &a, const Message &b) {
  if (a.header() == b.header()) {
    return false;
  }

  if (a.body_count() != b.body_count()) {
    return false;
  }

  auto a_it = a.begin();
  auto a_end = a.end();
  auto b_it = b.begin();

  while (a_it != a_end) {
    if (!(*a_it->get() == *b_it->get())) {
      return false;
    }

    ++a_it;
    ++b_it;
  }

  return true;
}

} // namespace messages
} // namespace neuro
