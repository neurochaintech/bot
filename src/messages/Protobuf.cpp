#include "messages/Protobuf.hpp"
#include <google/protobuf/util/json_util.h>
#include "common_types.hpp"
#include "crypto/Ecc.hpp"
#include "messages/Message.hpp"
#include "logger.hpp"

namespace neuro {
namespace messages {

bool Protobuf::parse_hello(const proto::Hello &proto, Message *message) {
  if (proto.has_listen_port()) {
    message->insert(std::make_shared<Hello>(proto.listen_port()));
  } else {
    message->insert(std::make_shared<Hello>());
  }

  return true;
}

bool Protobuf::parse_world(const proto::World &proto, Message *message) {
  bool res = true;
  auto world = std::make_shared<World>();
  if (proto.has_accepted()) {
    world->accepted = proto.accepted();
  }
  if (proto.has_request_id()) {
    world->request_id = proto.request_id();
  }

  for (const auto proto_peer : proto.peer_list()) {
    if (proto_peer.ip().has_ip4()) {
      networking::IP4 ip4(static_cast<uint32_t>(proto_peer.ip().ip4()));
      world->peers.emplace_back(std::make_shared<networking::Peer>(ip4, proto_peer.port()));
    } else if (proto_peer.ip().has_ip6()) {
      LOG_INFO << "IPv6 not implemented yet";
      // networking::IP6 ip6(*proto_peer.ip().ip6().data());
      // world->peers.emplace_back(ip4,
      // 				proto_peer.port());
      // world->peers.emplace_back();
    } else {
      LOG_ERROR << "No IP set in peer";
      res = false;
    }
  }
  LOG_DEBUG << "Inserted";
  message->insert(world);

  return res;
}

std::shared_ptr<Message> Protobuf::parse(const Buffer &buffer) {
  _reader.Clear();
  _reader.ParseFromArray(buffer.data(), buffer.size());
  std::string buff;
  google::protobuf::util::MessageToJsonString(_reader, &buff);
  auto message = std::make_shared<Message>();

  // Header parsing
  auto header = message->header();
  header->version(_reader.version());
  header->id(_reader.id());
  if (_reader.sender_key().type() ==
      static_cast<decltype(_reader.sender_key().type())>(
          crypto::keys::Type::EC)) {
    header->sender_key(_reader.sender_key().data());
  } else {
    LOG_ERROR << "Could not parse message";
    return {};
  }

  // Inner messages/bodys
  if (_reader.has_msg_hello()) {
    parse_hello(_reader.msg_hello(), message.get());
  }

  if (_reader.has_msg_world()) {
    parse_world(_reader.msg_world(), message.get());
  }

  return message;
}

void Protobuf::serialize(proto::Message *writer, const Body *body) const {
  switch (body->type) {
    case messages::Type::HELLO: {
      const auto *hello_msg = static_cast<const Hello *>(body);
      auto hello_proto = writer->mutable_msg_hello();
      hello_proto->set_listen_port(hello_msg->listen_port);

      break;
    }
    case messages::Type::WORLD: {
      const auto *world_msg = static_cast<const World *>(body);
      auto world_proto = writer->mutable_msg_world();
      world_proto->set_accepted(world_msg->accepted);
      world_proto->set_request_id(world_msg->request_id);
      for (const auto &peer : world_msg->peers) {
        if (peer->ip().is_v4()) {
          auto peer_proto = world_proto->add_peer_list();
          auto ip = peer_proto->mutable_ip();
          ip->set_ip4(peer->ip().to_v4().to_uint());
          peer_proto->set_port(peer->port());
        }
      }
      break;
    }
    default:
      LOG_ERROR << "Message type not handled";
  }
}

void Protobuf::load(const messages::Message &message) {
  for (const auto body : message) {
    serialize(&_writer, body.get());
  }

  // id
  _writer.set_id(message.header()->id());

  // version
  _writer.set_version(_version);

  // sender key
  auto proto_sender_key = _writer.mutable_sender_key();
  proto_sender_key->set_type(static_cast<uint8_t>(crypto::keys::Type::EC));
  proto_sender_key->set_data(message.header()->sender_key().data(),
                             message.header()->sender_key().size());
}

std::shared_ptr<Buffer> Protobuf::serialize(const messages::Message &message) {
  _writer.Clear();
  load(message);
  const auto size = _writer.ByteSizeLong();
  auto buffer = std::make_unique<Buffer>(size, 0);
  const auto res = _writer.SerializeToArray(buffer->data(), buffer->size());
  if (!res) {
    LOG_ERROR << "Could not serialize message";
    return {};
  }

  return std::move(buffer);
}

std::string Protobuf::to_json(const messages::Message &message) {
  load(message);
  std::string buff;
  google::protobuf::util::MessageToJsonString(_writer, &buff);
  return buff;
}

}  // namespace messages
}  // namespace neuro
