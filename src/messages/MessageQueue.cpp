#include "MessageQueue.hpp"

namespace neuro {
namespace messages {

MessageQueue::MessageQueue() {}

MessageQueue::~MessageQueue() {
  if (_started) {
    quit();
  }
}

void MessageQueue::publish(std::shared_ptr<const messages::Message> message) {
  {
    std::lock_guard<std::mutex> lock_queue(_queue_mutex);
    _queue.push(std::move(message));
  }
  _condition.notify_all();
}

void MessageQueue::subscribe(const messages::Type type,
                             MessageQueue::Callback function) {
  std::lock_guard<std::mutex> lock_callbacks(_callbacks_mutex);
  _callbacks[type].push_back(function);
}

void MessageQueue::run() {
  _started = true;
  _main_thread = std::thread([this]() { this->do_work(); });
}

bool MessageQueue::is_empty() {
  std::lock_guard<std::mutex> lock_queue(_queue_mutex);
  return _queue.empty();
}

std::shared_ptr<const messages::Message> MessageQueue::next_message() {
  std::lock_guard<std::mutex> lock_queue(_queue_mutex);
  auto message = _queue.front();
  _queue.pop();
  return message;
}

void MessageQueue::quit() {
  _quit = true;
  _condition.notify_all();
  _main_thread.join();
}
void MessageQueue::do_work() {
  do {
    while (!_quit && this->is_empty()) {  // avoid spurious wakeups
      std::unique_lock<std::mutex> lock_queue(_queue_mutex);
      _condition.wait(lock_queue);
    }
    // we validate again that the woke up call was not because it is quitting
    if (_quit) {
      break;
    }
    auto message = this->next_message();
    auto header = message->shr_ptr_header();
    // for every body in the message we get the type
    for (const auto &body : *message) {
      std::vector<MessageQueue::Callback> functions_list;
      {
        std::lock_guard<std::mutex> lock_callbacks(_callbacks_mutex);
        functions_list = _callbacks[body->type];
      }
      for (auto &func : functions_list) {
        // TODO this should take a worker from a pool of threads to run
        func(header, body);
      }
    }
  } while (!_quit);
}

}  // namespace messages
}  // namespace neuro
