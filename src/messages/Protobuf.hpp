#ifndef NEURO_SRC_MESSAGES_PROTOBUF_HPP
#define NEURO_SRC_MESSAGES_PROTOBUF_HPP

#include "Buffer.hpp"
#include "Messages.pb.h"
#include "messages/Parser.hpp"
#include "logger.hpp"

namespace neuro {
namespace messages {
struct Body;

class Protobuf : public Parser {
 private:
  const uint32_t _version;
  proto::Message _writer;
  proto::Message _reader;

  void serialize(proto::Message *writer, const Body *body) const;
  bool parse_hello(const proto::Hello &proto_hello, Message *message);
  bool parse_world(const proto::World &proto, Message *message);
  void load(const messages::Message &message);

 public:
  // TODO remove magic numbers
  Protobuf() : _version(1) {}

  std::shared_ptr<Message> parse(const Buffer &buffer);
  std::shared_ptr<Buffer> serialize(const messages::Message &message);
  std::string to_json(const messages::Message &message);
};

}  // namespace messages
}  // namespace neuro

#endif /* NEURO_SRC_MESSAGES_PROTOBUF_HPP */
