#ifndef NEURO_SRC_MESSAGES_MESSAGE_HPP
#define NEURO_SRC_MESSAGES_MESSAGE_HPP

#include <memory>
#include <optional>

#include "common_types.hpp"
#include "crypto/Hash.hpp"
#include "networking/Connection.hpp"
#include "networking/Peer.hpp"
#include "networking/TransportLayer.hpp"

namespace neuro {
namespace messages {

using Version = uint16_t;

enum Type { CONNECTION, DECONNECTION, HELLO, WORLD, _NB_ELEMENTS };

class Header {
public:
  using ID = uint32_t;

private:
  messages::Version _version;
  ID _id;
  bool _verified;

  Buffer _sender_key;
  networking::TransportLayer::ID _transport_layer_id;
  networking::Connection::ID _connection_id;

  /// When sending a message, header is filled by Transport Layer
public:
  Header()
      : _version(0), // version and id will be set by parser
        _id(0), _sender_key() {}

  Header(const Header &header) = default;

  Header(const messages::Version version, const ID id, const Buffer &sender_key)
      : _version(version), _id(id), _sender_key(sender_key) {}

  messages::Version version() const { return _version; }
  void version(const messages::Version version) { _version = version; }
  ID id() const { return _id; }
  void id(const ID id) { _id = id; }
  bool verified() const { return _verified; }
  void verified(const bool verified) { _verified = verified; }
  const Buffer &sender_key() const { return _sender_key; }
  Buffer *mutable_sender_key() { return &_sender_key; }
  void sender_key(const Buffer &key) { _sender_key.copy(key); }

  void connection_id(const networking::Connection::ID id) {
    _connection_id = id;
  }

  networking::Connection::ID connection_id() const { return _connection_id; }

  void transport_layer_id(const networking::TransportLayer::ID id) {
    _transport_layer_id = id;
  }

  networking::TransportLayer::ID transport_layer_id() const {
    return _transport_layer_id;
  }
};

struct Body {
  Type type;
  Body(const Type type) : type(type) {}
  ~Body() {}
};

struct Connection : public Body {
  /// true if remote connected to us, false if we connected to remote
  const bool from_remote;
  Connection(bool from_remote)
      : Body(Type::CONNECTION), from_remote(from_remote) {}
};

struct Deconnection : public Body {
  std::shared_ptr<networking::Peer> remote_peer;
  Deconnection(std::shared_ptr<networking::Peer> remote_peer)
      : Body(Type::DECONNECTION), remote_peer(remote_peer) {}
};

struct Hello : public Body {
  const Port listen_port;
  Hello() : Body(Type::HELLO), listen_port(0) {}
  Hello(const Port listen_port) : Body(Type::HELLO), listen_port(listen_port) {}
  ~Hello() {}
};

struct World : public Body {
  bool accepted;
  networking::Peers peers;
  Header::ID request_id;

  World() : Body(Type::WORLD), accepted(false), peers(), request_id() {}
  World(const bool accepted, const networking::Peers peers,
        const Header::ID request_id)
      : Body(Type::WORLD), accepted(accepted), peers(std::move(peers)),
        request_id(request_id) {}
};

class Message {
private:
  std::shared_ptr<Header> _header;
  std::vector<std::shared_ptr<const Body>> _bodys;

public:
  Message() : _header(std::make_shared<Header>()) {}
  Message(std::shared_ptr<Header> header) : _header(header) {}
  Message(std::shared_ptr<const Header> header) {
    _header = std::make_shared<Header>(*header.get());
  }

  void insert(std::shared_ptr<const Body> body) { _bodys.push_back(body); }

  void header(std::shared_ptr<Header> header) { _header = header; }
  Header *header() { return _header.get(); }
  const Header *header() const { return _header.get(); }

  std::shared_ptr<const Header> shr_ptr_header() const { return _header; }

  std::shared_ptr<const Body> body(const Type type);
  std::size_t body_count() const { return _bodys.size(); }
  decltype(_bodys.cbegin()) begin() const { return _bodys.cbegin(); }
  decltype(_bodys.cend()) end() const { return _bodys.cend(); }
};

std::ostream &operator<<(std::ostream &os, const Message &message);

bool operator==(const Header &a, const Header &b);
bool operator==(const Body &a, const Body &b);
bool operator==(const Hello &a, const Hello &b);
bool operator==(const World &a, const World &b);
bool operator==(const Message &a, const Message &b);

} // namespace messages
} // namespace neuro

#endif /* NEURO_SRC_MESSAGES_MESSAGE_HPP */
