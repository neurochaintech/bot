#ifndef NEURO_SRC_MESSAGES_PARSER_HPP
#define NEURO_SRC_MESSAGES_PARSER_HPP

#include "Buffer.hpp"

namespace neuro {
namespace crypto {
class Ecc;
}  // namespace crypto

namespace messages {
class Message;
class MessageQueue;
  
class Parser {
 protected:

 public:
  Parser() {}
  static std::shared_ptr<Parser> create(const std::string &name);
  virtual std::shared_ptr<Message> parse(const Buffer &) = 0;

  virtual std::shared_ptr<Buffer> serialize(
      const messages::Message &message) = 0;
  // virtual void serialize(const messages::Message *message,
  // uint8_t *data, std::size_t size) = 0; // TODO

  virtual ~Parser() {}
};

}  // namespace messages
}  // namespace neuro

#endif /* NEURO_SRC_MESSAGES_PARSER_HPP */
