
#include "messages/Protobuf.hpp"
#include "logger.hpp"

namespace neuro {
namespace messages {

std::shared_ptr<Parser> Parser::create(const std::string &name) {
  if(name == "protobuf") {
    return std::make_shared<Protobuf>();
  } else {
    LOG_ERROR << "Uknown parser name (" << name << ")";
    return {};
  }
}

}  // messages
}  // neuro

