#ifndef NEURO_SRC_MESSAGES_MESSAGEQUEUE_HPP
#define NEURO_SRC_MESSAGES_MESSAGEQUEUE_HPP

#include <array>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <queue>
#include <thread>

#include "messages/Message.hpp"
#include "logger.hpp"

namespace neuro {
namespace messages {

namespace test {
class MessageQueueTest;
}

/*!
   \Class MessageQueue
	 \brief Principal class used to handle Pub/Sub messages distribution in the network
 */
class MessageQueue {
 public:
  using Callback =
    std::function<void(std::shared_ptr<const messages::Header>, std::shared_ptr<const messages::Body>)>;

 protected:
	/*!
		\fn std::shared_ptr<const messages::Message> MessageQueue::next_message()
		\brief Protected method to retrieve from the queue the next message to trait
		\return A shared pointer to a constant messages::Message
	 */
  std::shared_ptr<const messages::Message> next_message();
	/*!
		\fn bool MessageQueue::is_empty() const
		\brief Protected method to check if the message queue is empty or not
		\return True if there is no message in _queue, false otherwise
	 */
  bool is_empty();
  /*!
		\fn bool MessageQueue::quit() const
		\brief Protected method to set the quitting flag.

		It is only call from the destructor and set the quitting flag so the MessageQueue object
		can go out of the main loop and quit its thread.
	 */
  void quit();
  /*!
		\fn bool MessageQueue::do_work()
		\brief Protected method that has the main loop of the MessageQueue

		It is called from the MessageQueue::run() to enter the MessageQueue thread and start its main loop
	 */
  void do_work();

 private:
  bool _started{false};
	/*!
		\var std::array<std::vector<MessageQueue::Callback>, messages::Type::_NB_ELEMENTS>
      _callbacks
		\brief An array that has a vector of callbacks for every type of messages::Message
		\note Maybe it should be implemented using a std::set to avoid inserting the same callback more than once
	 */
  std::array<std::vector<MessageQueue::Callback>, messages::Type::_NB_ELEMENTS>
      _callbacks;
	/*!
		\var std::queue<std::shared_ptr<const messages::Message>> _queue
		\brief Main queue with all the messages::Message that has been pushed and need to be distributed
	 */
  std::queue<std::shared_ptr<const messages::Message>> _queue;
	/*!
		\var std::queue<std::shared_ptr<const messages::Message>> _queue
		\brief
	 */
  std::mutex _queue_mutex;
	/*!
		\var std::queue<std::shared_ptr<const messages::Message>> _queue
		\brief
	 */
  mutable std::mutex _callbacks_mutex;
	/*!
		\var std::queue<std::shared_ptr<const messages::Message>> _queue
		\brief
	 */
  std::atomic<bool> _quit{false};
  std::thread _main_thread;
  /*!
                \var std::condition_variable _condition;
                \brief
  */
  std::condition_variable _condition;

 public:
  MessageQueue();
  ~MessageQueue();

  /*!
		\fn void MessageQueue::publish(std::shared_ptr<const messages::Message> msg)
		\param msg A shared pointer to a messages::Message (wich might have several bodies)
		\brief Public method used for distributing (pusblish) a messages::Message
	 */
  void publish(std::shared_ptr<const messages::Message>);
  /*!
		\fn void MessageQueue::subscribe(const messages::Type type, MessagesQueue::Callback callback)
		\param type Type of message to which you want to subscribe
		\param callback A function returning void, receiving a shared pointer to a constant messages::Message. This is the function to be executed everytime a message of given type arrives.
		\brief Public method used to subscribe to a given type of messages
	 */
  void subscribe(const messages::Type, MessageQueue::Callback);

  /*!
    \fn void MessageQueue::run()
    \brief Public method that need to me called in order for the MessageQueue to start processing the pushed messages for the subscribers.
  */
  void run();

  friend class neuro::messages::test::MessageQueueTest;
};

}  // namespace messages
}  // namespace neuro

#endif /* NEURO_SRC_MESSAGE_QUEUE_HPP */
