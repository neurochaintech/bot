#include "logger.hpp"
#include "config/Logs.hpp"

BOOST_LOG_GLOBAL_LOGGER_INIT(logger, src::severity_logger_mt) {
  src::severity_logger_mt<boost::log::trivial::severity_level> logger;
  // add global attributes
  logger.add_attribute("TimeStamp", attrs::local_clock());
  return logger;
}

namespace neuro {
namespace log {

logging::formatter neuro_formatter() {
  return expr::stream << expr::format_date_time(timestamp,
                                                "%Y-%m-%d %H:%M:%S")
                      << " [" << logging::trivial::severity << "] "
                      << expr::smessage;
}

void add_file_sink(const std::string &log_file, const size_t rotation_size_mb) {
  auto backend = boost::make_shared<sinks::text_file_backend>(
      // file name pattern
      keywords::file_name = log_file,
      // rotate the file upon reaching `rotation_size_mb` MiB size...
      keywords::rotation_size = rotation_size_mb * 1024 * 1024);

  auto sink = boost::make_shared<file_sink>(backend);
  //boost::shared_ptr<file_sink> sink(new file_sink(backend));
  sink->set_formatter(neuro_formatter());
  sink->locked_backend()->auto_flush(true);
  logging::core::get()->add_sink(sink);
}

void add_stdout_sink() {
  auto sink = boost::make_shared<text_sink>();
  sink->locked_backend()->add_stream(
      boost::shared_ptr<std::ostream>(&std::clog, boost::null_deleter()));

  sink->set_formatter(neuro_formatter());
  sink->locked_backend()->auto_flush(true);
  logging::core::get()->add_sink(sink);
}

  void from_config(const config::Logs &logs_cfg) {
  if (logs_cfg.stdout()) {
    add_stdout_sink();
  }

  const std::string log_file = logs_cfg.file_path();
  if (!log_file.empty()) {
    add_file_sink(log_file, logs_cfg.rotation_size_mb());
  }

  const std::string log_severity = logs_cfg.severity();
  if (!log_severity.empty()) {
    if (log_severity == "trace") {
      logging::core::get()->set_filter(logging::trivial::severity >=
                                       logging::trivial::trace);
    } else if (log_severity == "debug") {
      logging::core::get()->set_filter(logging::trivial::severity >=
                                       logging::trivial::debug);
    } else if (log_severity == "info") {
      logging::core::get()->set_filter(logging::trivial::severity >=
                                       logging::trivial::info);
    } else if (log_severity == "warning") {
      logging::core::get()->set_filter(logging::trivial::severity >=
                                       logging::trivial::warning);
    } else if (log_severity == "error") {
      logging::core::get()->set_filter(logging::trivial::severity >=
                                       logging::trivial::error);
    } else if (log_severity == "fatal") {
      logging::core::get()->set_filter(logging::trivial::severity >=
                                       logging::trivial::fatal);
    } else {
      logging::core::get()->set_filter(logging::trivial::severity >=
                                       logging::trivial::info);
    }
  } else {
    logging::core::get()->set_filter(logging::trivial::severity >=
                                     logging::trivial::info);
  }
}

} // namespace log
} // namespace neuro
