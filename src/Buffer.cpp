#include "Buffer.hpp"
#include <iostream>
#include <iterator>

namespace neuro {

std::ostream &operator<<(std::ostream &os, const Buffer &buffer) {
  std::ios_base::fmtflags f(os.flags());
  os << std::hex;
  std::copy(buffer.cbegin(), buffer.cend(),
            std::ostream_iterator<int>(os, ""));
  os.flags(f);

  return os;
}

}  // namespace neuro
