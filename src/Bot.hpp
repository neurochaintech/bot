#ifndef NEURO_SRC_BOT_HPP
#define NEURO_SRC_BOT_HPP

#include <memory>
#include <unordered_map>

#include "config/Bot.hpp"
#include "crypto/Ecc.hpp"
#include "networking/Networking.hpp"
#include "networking/Peer.hpp"
#include "networking/tcp/Tcp.hpp"
#include "common_types.hpp"

namespace neuro {

class Bot {
private:
  networking::Peers _peers{};
  std::size_t _connected_peers{0};
  std::size_t _max_connections;
  std::shared_ptr<config::Bot> _conf;
  std::shared_ptr<config::Peers> _peers_conf;
  std::shared_ptr<crypto::Ecc> _keys;
  std::shared_ptr<networking::Networking> _networking;
  std::shared_ptr<networking::Tcp> _tcp;
  networking::Peer::Status _keep_status;

  bool init();
  void handler_hello(std::shared_ptr<const messages::Header> header,
                     std::shared_ptr<const messages::Body> hello);
  void handler_world(std::shared_ptr<const messages::Header> header,
                     std::shared_ptr<const messages::Body> hello);
  void handler_connection(std::shared_ptr<const messages::Header> header,
                          std::shared_ptr<const messages::Body> body);
  void handler_deconnection(std::shared_ptr<const messages::Header> header,
                            std::shared_ptr<const messages::Body> body);
  bool next_to_connect(std::shared_ptr<networking::Peer> &out_peer);

public:

  Bot(std::istream &bot_stream, std::istream &peers_stream);
  Bot(const std::string &configuration_path);
  Bot(std::shared_ptr<config::Bot> bot, std::shared_ptr<config::Peers> peers);
  Bot(const Bot &) = delete;
  Bot(Bot &&) = delete;
  networking::Networking *networking();
  networking::Tcp *tcp();
  void stop();
  void join();
  void status() const;
  void keep_max_connections();
  void update_peers_file(const std::string &filename) const;
  ~Bot();
};

} // namespace neuro

#endif /* NEURO_SRC_BOT_HPP */
