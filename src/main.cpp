#include <boost/program_options.hpp>
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include "Bot.hpp"
#include "common_types.hpp"
#include "config/Bot.hpp"
#include "logger.hpp"

namespace neuro {

namespace po = boost::program_options;
using namespace std::chrono_literals;

int main(int argc, char *argv[]) {
  po::options_description desc("Allowed options");
  desc.add_options()("help,h", "Produce help message.")(
      "configuration,c", po::value<std::string>()->default_value("bot.json"),
      "Configuration path.");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  try {
    po::notify(vm);
  } catch (po::error &e) {
    LOG_WARNING << desc;
    return 1;
  }

  if (vm.count("help")) {
    LOG_INFO << desc;
    return 1;
  }

  Bot bot(vm["configuration"].as<std::string>());
  bot.join();

  return 0;
}
}  // namespace neuro

int main(int argc, char *argv[]) {
  //
  return neuro::main(argc, argv);
}
