#ifndef NEURO_SRC_NETWORKING_TRANSPORT_LAYER_HPP
#define NEURO_SRC_NETWORKING_TRANSPORT_LAYER_HPP

#include <thread>

#include "common_types.hpp"
#include "logger.hpp"
#include "messages/Parser.hpp"
#include "networking/Peer.hpp"

namespace neuro {
namespace messages {
class MessageQueue;
}  // namespace messages

namespace crypto {
class Ecc;
}  // namespace crypto

namespace networking {
class Networking;

class TransportLayer {
 public:
  using ID = int16_t;

 protected:
  Networking *_networking;
  crypto::Ecc *_keys;
  std::shared_ptr<messages::Parser> _parser;
  ID _id;
  std::thread _thread;

 protected:
  virtual void _run() = 0;
  virtual void _stop() = 0;
  virtual std::size_t _peer_count() const = 0;

 public:
  TransportLayer(Networking *networking, crypto::Ecc *keys,
                 std::shared_ptr<messages::Parser> parser);

  virtual bool send(const std::shared_ptr<messages::Message> message) = 0;
  virtual bool send_unicast(
      const std::shared_ptr<messages::Message> message) = 0;
  virtual void peers(Peers *peers) const = 0;
  ID id() const;
  void id(const ID id);
  void run();
  void stop();
  std::size_t peer_count() const;
  void join();
  virtual ~TransportLayer();
};

}  // namespace networking

}  // namespace neuro

#endif /* NEURO_SRC_TRANSPORT_LAYER_HPP */
