#ifndef NEURO_SRC_NETWORKING_NETWORKING_HPP
#define NEURO_SRC_NETWORKING_NETWORKING_HPP

#include <memory>
#include <random>
#include <vector>

#include "messages/MessageQueue.hpp"
#include "networking/TransportLayer.hpp"

namespace neuro {
namespace networking {

class Networking {
 private:
  std::vector<std::shared_ptr<TransportLayer>> _transport_layers;
  messages::MessageQueue _message_queue;

  std::random_device _rd;
  std::uniform_int_distribution<int> _dist;

 public:
  Networking(const std::size_t max_connections);

  TransportLayer::ID push(std::shared_ptr<TransportLayer> transport_layer);
  void publish(std::shared_ptr<messages::Message> message);
  void subscribe(const messages::Type, messages::MessageQueue::Callback);
  void send(std::shared_ptr<messages::Message> message);
  void send_unicast(std::shared_ptr<messages::Message> message);
  messages::MessageQueue *message_queue();
  Peers peers() const;

  void stop();
  void join();
};

}  // namespace networking
}  // namespace neuro

#endif /* NEURO_SRC_NETWORKING_HPP */
