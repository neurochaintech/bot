#ifndef NEURO_SRC_NETWORKING_TCP_TCP_HPP
#define NEURO_SRC_NETWORKING_TCP_TCP_HPP

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <unordered_map>

#include "common_types.hpp"
#include "networking/Connection.hpp"
#include "networking/TransportLayer.hpp"
#include "networking/tcp/Connection.hpp"

namespace neuro {
namespace messages {
class MessageQueue;
class Parser;
} // namespace messages

namespace networking {

namespace test {
class Tcp;
} // namespace test

class Tcp : public TransportLayer {
private:
  boost::asio::io_service _io_service;
  bai::tcp::resolver _resolver;
  Connection::ID _current_connection_id;
  std::unordered_map<Connection::ID, tcp::Connection> _connections;
  mutable std::mutex _connection_mutex;
  std::atomic<bool> _stopping{false};
  Port _listening_port{0};
  IP _local_ip{};
  mutable std::mutex _stopping_mutex;

  void _run();
  void _stop();
  std::size_t _peer_count() const;
  void new_connection(std::shared_ptr<bai::tcp::socket> socket,
                      const boost::system::error_code &error,
                      std::shared_ptr<networking::Peer> peer,
                      const bool from_remote);
  void accept(std::shared_ptr<bai::tcp::acceptor> acceptor, const Port port);

public:
  Tcp(Tcp &&) = delete;
  Tcp(const Tcp &) = delete;

  Tcp(Networking *networking, crypto::Ecc *keys,
      std::shared_ptr<messages::Parser> parser);
  void accept(const Port port);
  void connect(const std::string &host, const std::string &service);
  void connect(const std::shared_ptr<networking::Peer> peer);
  bool connect(const bai::tcp::endpoint host, const Port port);
  bool send(std::shared_ptr<messages::Message> message);
  bool send_unicast(std::shared_ptr<messages::Message> message);
  bool disconnected(const Connection::ID id, std::shared_ptr<Peer> remote_peer);
  void peers(Peers *peers) const;
  Port listening_port() const;
  IP local_ip() const;
  std::shared_ptr<Peer> remote_peer(const Connection::ID id) const;
  const IP remote_ip(const Connection::ID id) const;
  const Port remote_port(const Connection::ID id) const;
  ~Tcp();

  friend class neuro::networking::test::Tcp;
};

} // namespace networking
} // namespace neuro

#endif /* NEURO_SRC_NETWORKING_TCP_TCP_HPP */
