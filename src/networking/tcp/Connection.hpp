#ifndef NEURO_SRC_NETWORKING_TCP_CONNECTION_HPP
#define NEURO_SRC_NETWORKING_TCP_CONNECTION_HPP

#include <boost/asio.hpp>
#include <cmath>
#include <iostream>
#include <stdio.h>

#include "messages/Parser.hpp"
#include "networking/Connection.hpp"
#include "networking/tcp/HeaderPattern.hpp"
#include "networking/Peer.hpp"

namespace neuro {
namespace messages {
class MessageQueue;
} // namespace messages
namespace networking {
class Tcp;
namespace tcp {
using boost::asio::ip::tcp;

class Connection : public networking::Connection {
private:
  networking::Connection::ID _id;
  Tcp *_tcp;
  messages::MessageQueue *_message_queue;
  messages::Parser *_parser;
  std::shared_ptr<tcp::socket> _socket;
  Buffer _header;
  Buffer _message;
  Port _listen_port;
  std::shared_ptr<Peer> _remote_peer;
  mutable std::mutex _connection_mutex;

  void terminate();

public:
  Connection(const ID id, Tcp *tcp, messages::MessageQueue *message_queue,
             messages::Parser *parser, std::shared_ptr<tcp::socket> socket,
             std::shared_ptr<Peer> remote_peer, const bool from_remote)
      : _id(id), _tcp(tcp), _message_queue(message_queue),
        _parser(std::move(parser)), _socket(std::move(socket)),
        _header(sizeof(HeaderPattern), 0), _message(128, 0),
        _remote_peer(std::move(remote_peer)) {
    _listen_port = _remote_peer->port();
  }

  std::shared_ptr<tcp::socket> socket();
  ID id() const;

  void read();

  void read_header();

  void read_body();
  bool send(const Buffer &message);
  const std::shared_ptr<Peer> peer() const;
  const IP remote_ip() const;
  const Port remote_port() const;
};
} // namespace tcp
} // namespace networking
} // namespace neuro

#endif /* NEURO_SRC_NETWORKING_CONNECTION_HPP */
