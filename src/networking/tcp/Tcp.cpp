#include <chrono>
#include <sstream>
#include <thread>
#include <tuple>

#include "crypto/Ecc.hpp"
#include "logger.hpp"
#include "messages/Protobuf.hpp"
#include "networking/Networking.hpp"
#include "networking/tcp/HeaderPattern.hpp"
#include "networking/tcp/Tcp.hpp"

namespace neuro {
namespace networking {
using namespace std::chrono_literals;

Tcp::Tcp(Networking *networking, crypto::Ecc *keys,
         std::shared_ptr<messages::Parser> parser)
    : TransportLayer(networking, keys, parser), _io_service(),
      _resolver(_io_service), _current_connection_id(0) {}

bool Tcp::connect(const bai::tcp::endpoint host, const Port port) {
  return false; // TODO
}

void Tcp::connect(const std::shared_ptr<networking::Peer> peer) {
  bai::tcp::resolver resolver(_io_service);
  bai::tcp::resolver::query query(peer->ip().to_string(),
                                  std::to_string(peer->port()));
  bai::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

  auto socket = std::make_shared<bai::tcp::socket>(_io_service);
  auto handler = [this, socket, peer](boost::system::error_code error,
                                      bai::tcp::resolver::iterator iterator) {
    this->new_connection(std::move(socket), error, std::move(peer), false);
  };
  boost::asio::async_connect(*socket, endpoint_iterator, handler);
}

void Tcp::connect(const std::string &host, const std::string &service) {}

void Tcp::accept(const Port port) {
  auto acceptor = std::make_shared<bai::tcp::acceptor>(
      _io_service, bai::tcp::endpoint(bai::tcp::v4(), port));
  accept(acceptor, port);
}

void Tcp::accept(std::shared_ptr<bai::tcp::acceptor> acceptor,
                 const Port port) {
  _listening_port = port;
  _local_ip = acceptor->local_endpoint().address();
  auto peer = std::make_shared<networking::Peer>(_local_ip.to_string(), port);
  auto socket = std::make_shared<bai::tcp::socket>(acceptor->get_io_service());
  acceptor->async_accept(*socket, [this, acceptor, socket, peer, port](
                                      const boost::system::error_code &error) {
    this->new_connection(std::move(socket), error, std::move(peer), true);
    this->accept(std::move(acceptor), port);
  });
}

Port Tcp::listening_port() const { return _listening_port; }
IP Tcp::local_ip() const { return _local_ip; }

const IP Tcp::remote_ip(const Connection::ID id) const {
  std::lock_guard<std::mutex> lock_queue(_connection_mutex);
  const auto connection_it = _connections.find(id);
  if (connection_it == _connections.end()) {
    LOG_ERROR << "Could not find the connection_id. Returning empty IP";
    return IP{};
  }
  return connection_it->second.remote_ip();
}

const Port Tcp::remote_port(const Connection::ID id) const {
  std::lock_guard<std::mutex> lock_queue(_connection_mutex);
  const auto connection_it = _connections.find(id);
  if (connection_it == _connections.end()) {
    LOG_ERROR << "Could not find the connection_id. Returning Port{0}";
    return Port{0};
  }
  return connection_it->second.remote_port();
}

std::shared_ptr<Peer> Tcp::remote_peer(const Connection::ID id) const {
  std::lock_guard<std::mutex> lock_queue(_connection_mutex);
  const auto connection_it = _connections.find(id);
  if (connection_it == _connections.end()) {
    LOG_ERROR << "Could not find the connection_id. Returning empty Peer";
    return std::shared_ptr<Peer>();
  }
  return connection_it->second.peer();
}

void Tcp::new_connection(std::shared_ptr<bai::tcp::socket> socket,
                         const boost::system::error_code &error,
                         std::shared_ptr<networking::Peer> peer,
                         const bool from_remote) {
  std::lock_guard<std::mutex> lock_queue(_connection_mutex);
  auto header = std::make_shared<messages::Header>();
  header->transport_layer_id(id());
  if (!error) {
    _current_connection_id++;
    header->connection_id(_current_connection_id);
    auto r = _connections.emplace(
        std::piecewise_construct, std::forward_as_tuple(_current_connection_id),
        std::forward_as_tuple(_current_connection_id, this,
                              _networking->message_queue(), _parser.get(),
                              std::move(socket), std::move(peer), from_remote));

    auto message = std::make_shared<messages::Message>(header);
    message->insert(std::make_shared<messages::Connection>(from_remote));
    _networking->publish(message);
    r.first->second.read();
  } else {
    LOG_WARNING << "Could not create new connection to " << *peer;
    auto message = std::make_shared<messages::Message>(header);
    message->insert(std::make_shared<messages::Deconnection>(peer));
    _networking->publish(message);
  }
}

void Tcp::_run() {
  LOG_INFO << "Starting io_service";
  boost::system::error_code ec;
  {
    std::lock_guard<std::mutex> m(_stopping_mutex);
    if (_stopping) {
      return;
    }
  }
  _io_service.run(ec);
  if (ec) {
    LOG_ERROR << "service run failed (" << ec.message() << ")";
  }
}

void Tcp::_stop() {
  {
    std::lock_guard<std::mutex> m(_stopping_mutex);
    _stopping = true;
  }
  _io_service.stop();
  while (!_io_service.stopped()) {
    std::this_thread::sleep_for(10ms);
  }
}

bool Tcp::send(std::shared_ptr<messages::Message> message) {
  std::lock_guard<std::mutex> lock_queue(_connection_mutex);

  if (_connections.size() == 0) {
    LOG_ERROR << "Could not send mesasge because there is no connection";
    return false;
  }
  _keys->public_key().save(message->header()->mutable_sender_key());
  const auto buffer = _parser->serialize(*message.get());
  if (buffer.get() == nullptr) {
    LOG_ERROR << "Not sending message because couldn't serialize";
    return false;
  }

  bool res = true;
  if (buffer->size() > (1 << (8 * sizeof(tcp::HeaderPattern::size)))) {
    LOG_ERROR << "Message is too big (" << buffer->size() << ")";
    return false;
  }

  Buffer header(sizeof(tcp::HeaderPattern), 0);
  auto header_pattern = reinterpret_cast<tcp::HeaderPattern *>(header.data());
  header_pattern->size = buffer->size();
  _keys->sign(buffer->data(), buffer->size(),
              reinterpret_cast<uint8_t *>(&header_pattern->signature));

  for (auto &connection : _connections) {
    res &= connection.second.send(header);
    res &= connection.second.send(*buffer);
  }

  return res;
}

bool Tcp::send_unicast(std::shared_ptr<messages::Message> message) {
  std::lock_guard<std::mutex> lock_queue(_connection_mutex);

  auto got = _connections.find(message->header()->connection_id());
  if (got == _connections.end()) {
    return false;
  }

  _keys->public_key().save(message->header()->mutable_sender_key());
  const auto buffer = _parser->serialize(*message.get());
  if (buffer.get() == nullptr) {
    LOG_ERROR << "Not sending message because couldn't serialize";
    return false;
  }

  if (buffer->size() > (1 << (8 * sizeof(tcp::HeaderPattern::size)))) {
    LOG_ERROR << "Message is too big (" << buffer->size() << ")";
    return false;
  }

  Buffer header(sizeof(tcp::HeaderPattern), 0);
  auto header_pattern = reinterpret_cast<tcp::HeaderPattern *>(header.data());
  header_pattern->size = buffer->size();
  _keys->sign(buffer->data(), buffer->size(),
              reinterpret_cast<uint8_t *>(&header_pattern->signature));

  got->second.send(header);
  got->second.send(*buffer);

  return true;
}

bool Tcp::disconnected(const Connection::ID id, std::shared_ptr<Peer> peer) {
  std::lock_guard<std::mutex> lock_queue(_connection_mutex);
  auto got = _connections.find(id);
  if (got == _connections.end()) {
    LOG_ERROR << "Connection not found";
    return false;
  }

  auto header = std::make_shared<messages::Header>();
  header->connection_id(id);
  header->transport_layer_id(this->id());

  auto message = std::make_shared<messages::Message>(header);
  message->insert(std::make_shared<messages::Deconnection>(peer));
  _networking->publish(message);
  LOG_INFO << "Socket deconnected";

  _connections.erase(got);
  return true;
}

std::size_t Tcp::_peer_count() const {
  std::lock_guard<std::mutex> lock_queue(_connection_mutex);
  return _connections.size();
}

void Tcp::peers(Peers *peers) const {
  std::lock_guard<std::mutex> lock_queue(_connection_mutex);

  for (const auto &connection : _connections) {
    peers->emplace_back(connection.second.peer());
  }
}

Tcp::~Tcp() {
  std::lock_guard<std::mutex> lock_queue(_connection_mutex);
  _stop();
}

} // namespace networking
} // namespace neuro
