#include "networking/tcp/Connection.hpp"
#include "logger.hpp"
#include "messages/MessageQueue.hpp"
#include "networking/tcp/Tcp.hpp"

namespace neuro {
namespace networking {
namespace tcp {

std::shared_ptr<tcp::socket> Connection::socket() { return std::move(_socket); }
Connection::ID Connection::id() const { return _id; }

void Connection::read() { read_header(); }

void Connection::read_header() {
  boost::asio::async_read(
      *_socket, boost::asio::buffer(_header.data(), _header.size()),
      [this](const boost::system::error_code &error, std::size_t bytes_read) {
        if (error) {
          this->terminate();
          return;
        }

        auto header_pattern = reinterpret_cast<HeaderPattern *>(_header.data());
        _message.resize(header_pattern->size);
        read_body();
      });
}
void Connection::read_body() {
  boost::asio::async_read(
      *_socket, boost::asio::buffer(_message.data(), _message.size()),
      [this](const boost::system::error_code &error, std::size_t bytes_read) {
        if (error) {
          this->terminate();
          return;
        }
        const auto message = _parser->parse(_message);
        if (const auto hello = message->body(messages::Type::HELLO); hello) {
          _listen_port =
              static_cast<const messages::Hello *>(hello.get())->listen_port;
        }
        auto header_pattern = reinterpret_cast<HeaderPattern *>(_header.data());

        crypto::EccPub key_pub(message->header()->sender_key());
        bool verified = key_pub.verify(_message, header_pattern->signature,
                                       sizeof(header_pattern->signature));
        message->header()->verified(verified);
        LOG_DEBUG << std::boolalpha << "verified " << verified;
        message->header()->connection_id(_id);
        message->header()->transport_layer_id(_tcp->id());
        if (verified) {
          _message_queue->publish(message);
        }

        read_header();
      });
}

bool Connection::send(const Buffer &message) {
  boost::asio::async_write(*_socket,
                           boost::asio::buffer(message.data(), message.size()),
                           [this](const boost::system::error_code &error,
                                  std::size_t bytes_transferred) {
                             if (error) {
                               LOG_ERROR << "Could not send message";
                               this->terminate();
                               return false;
                             }
                             return true;
                           });
  return true;
}

void Connection::terminate() { _tcp->disconnected(_id, _remote_peer); }

const IP Connection::remote_ip() const {
  const auto endpoint = _socket->remote_endpoint();
  return endpoint.address();
}

const Port Connection::remote_port() const {
  const auto endpoint = _socket->remote_endpoint();
  return static_cast<Port>(endpoint.port());
}

const std::shared_ptr<Peer> Connection::peer() const {
  return _remote_peer;
  // if we didn't connect to remote bot we can't know the remote port
}

} // namespace tcp
} // namespace networking
} // namespace neuro
