#include "networking/TransportLayer.hpp"
#include "logger.hpp"
#include "messages/Parser.hpp"
#include "networking/Networking.hpp"

namespace neuro {
namespace networking {

TransportLayer::TransportLayer(Networking *networking, crypto::Ecc *keys,
                               std::shared_ptr<messages::Parser> parser)
    : _networking(networking), _keys(keys), _parser(parser), _id(-1) {}

TransportLayer::ID TransportLayer::id() const { return _id; }
void TransportLayer::id(const TransportLayer::ID id) { _id = id; }

void TransportLayer::run() {
  _thread = std::thread ([this]() { this->_run(); });
}

void TransportLayer::stop() { this->_stop(); }

std::size_t TransportLayer::peer_count() const { return _peer_count(); }

void TransportLayer::join() { _thread.join(); }
TransportLayer::~TransportLayer() {
  if(_thread.joinable()){
      _thread.join();
  }
}

}  // namespace networking
}  // namespace neuro
