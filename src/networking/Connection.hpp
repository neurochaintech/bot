#ifndef NEURO_SRC_NETWORKING_CONNECTION_HPP
#define NEURO_SRC_NETWORKING_CONNECTION_HPP

#include "common_types.hpp"

namespace neuro {
namespace networking {

  class Connection {
  public:
    using ID = uint16_t;
  };

}  // namespace networking
}  // namespace neuro

#endif /* NEURO_NETWORKING_CONNECTION_HPP */
