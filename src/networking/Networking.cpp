#include <limits>
#include <string>

#include "networking/Networking.hpp"
#include "networking/Peer.hpp"
#include "logger.hpp"

namespace neuro {
namespace networking {

Networking::Networking(std::size_t max_connections)
    : _dist(0, std::numeric_limits<uint32_t>::max()) {
  _message_queue.run();
}

messages::MessageQueue *Networking::message_queue() { return &_message_queue; }

void Networking::send(std::shared_ptr<messages::Message> message) {
  message->header()->id(_dist(_rd));
  for (auto &transport_layer : _transport_layers) {
    LOG_DEBUG << "Sending to transport layer";
    transport_layer->send(message);
  }
}

void Networking::send_unicast(std::shared_ptr<messages::Message> message) {
  _transport_layers[message->header()->transport_layer_id()]->send_unicast(
      message);
}

void Networking::publish(std::shared_ptr<messages::Message> message) {
  _message_queue.publish(message);
}

void Networking::subscribe(const messages::Type type,
                           messages::MessageQueue::Callback callback) {
  _message_queue.subscribe(type, callback);
}

TransportLayer::ID Networking::push(std::shared_ptr<TransportLayer> transport_layer) {
  _transport_layers.push_back(transport_layer);
  transport_layer->run();
  const auto id = _transport_layers.size() - 1;
  transport_layer->id(id);
  return id;
}

void Networking::stop() {
  for (const auto transport_layer : _transport_layers) {
    transport_layer->stop();
  }
}

void Networking::join() {
  for (const auto transport_layer : _transport_layers) {
    transport_layer->join();
  }
}

Peers Networking::peers() const {
  Peers peers;
  for (const auto transport_layer : _transport_layers) {
    transport_layer->peers(&peers);
  }

  return peers;
}

}  // namespace networking
}  // namespace neuro
