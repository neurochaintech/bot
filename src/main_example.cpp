#include <boost/program_options.hpp>
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include "apps/Neurochain.hpp"
#include "common_types.hpp"
#include "config/Bot.hpp"
#include "crypto/Ecc.hpp"
#include "messages/Message.hpp"
#include "messages/Protobuf.hpp"
#include "networking/Networking.hpp"
#include "networking/tcp/Tcp.hpp"

namespace neuro {

namespace po = boost::program_options;
using namespace std::chrono_literals;

int main(int argc, char *argv[]) {
  po::options_description desc("Allowed options");
  desc.add_options()("help,h", "Produce help message.")(
      "listen,l", po::value<Port>(), "Listen on port.")(
      "connect,c", po::value<Port>(), "Connect on port.");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  try {
    po::notify(vm);
  } catch (po::error &e) {
    std::cout << desc << "\n";
    return 1;
  }

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  // Create new pair of keys
  crypto::Ecc keys;
  keys.save("private.der", "public.der");

  // Create networking engine
  networking::Networking networking;

  // Create parser (protobuf)
  messages::Protobuf parser;

  // Create a transport layer (tcp)
  // It needs keys to handle message integrity (sign/verify)
  networking::Tcp tcp(&networking, &keys, &parser);

  Port listen_port = 0;
  if (vm.count("listen")) {
    listen_port = vm["listen"].as<Port>();
    tcp.accept(listen_port);
  }

  if (vm.count("connect")) {
    tcp.connect("localhost", std::to_string(vm["connect"].as<Port>()));
  }

  // Push it to network engine
  networking.push(&tcp);

  std::this_thread::sleep_for(2s);
  apps::Neurochain neurochain(&networking);

  // Create generic message container
  auto message = std::make_shared<messages::Message>();

  // Let's be polite and say hello first.
  message->insert(std::make_shared<messages::Hello>(listen_port));
  networking.send(message);
  std::this_thread::sleep_for(2s);
  networking.join();

  return 0;
}
}  // namespace neuro

// namespace neuro

int main(int argc, char *argv[]) {
  //
  return neuro::main(argc, argv);
}
