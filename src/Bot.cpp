#include "Bot.hpp"
#include "common_types.hpp"
#include "config/Peers.hpp"
#include "logger.hpp"
#include "networking/Peer.hpp"
#include "networking/tcp/Tcp.hpp"
#include <algorithm>

namespace neuro {

Bot::Bot(std::istream &bot_stream, std::istream &peers_stream)
    : _conf(std::make_shared<config::Bot>(bot_stream)),
      _peers_conf(std::make_shared<config::Peers>(peers_stream)) {
  if (!init()) {
    throw std::runtime_error("Could not create bot from configuration file");
  }
}

Bot::Bot(const std::string &configuration_path)
    : _conf(std::make_shared<config::Bot>(configuration_path)) {
  if (!init()) {
    throw std::runtime_error("Could not create bot from configuration file");
  }
}
Bot::Bot(std::shared_ptr<config::Bot> conf,
         std::shared_ptr<config::Peers> peers_conf)
    : _conf(conf), _peers_conf(peers_conf) {
  if (!init()) {
    throw std::runtime_error("Could not create bot from configuration file");
  }
}

void Bot::handler_connection(std::shared_ptr<const messages::Header> header,
                             std::shared_ptr<const messages::Body> body) {
  auto connection = static_cast<const messages::Connection *>(body.get());
  auto connection_id = header->connection_id();
  auto remote_peer = _tcp->remote_peer(connection_id);

  if (!remote_peer) {
    LOG_ERROR << "Got an empty peer. Going out of the connection handler";
    return;
  }

  if (connection->from_remote) {
    // Nothing to do; just wait for the hello message from remote peer
    LOG_DEBUG << "Got a connection from " << *remote_peer;
  } else {
    LOG_DEBUG << "Got a connection to " << *remote_peer;
    auto response_msg = std::make_shared<messages::Message>();
    response_msg->header()->connection_id(connection_id);
    response_msg->header()->transport_layer_id(header->transport_layer_id());
    response_msg->insert(
        std::make_shared<messages::Hello>(_tcp->listening_port()));
    _networking->send_unicast(response_msg);
  }
}

void Bot::handler_deconnection(std::shared_ptr<const messages::Header> header,
                               std::shared_ptr<const messages::Body> body) {

  LOG_DEBUG << "Got a deconnection message";
  auto deconnection = static_cast<const messages::Deconnection *>(body.get());
  auto remote_peer = deconnection->remote_peer;
  using ps = networking::Peer::Status;

  // If I was trying to connect then it is not reachable, else it means I was
  // connected and it could be reachable
  auto old_status = remote_peer->status();
  if (old_status == ps::CONNECTING) {
    remote_peer->status(ps::UNREACHABLE);
  } else {
    remote_peer->status(ps::REACHABLE);
    _connected_peers -= 1;
  }
  this->status();
  this->keep_max_connections();
}

void Bot::handler_world(std::shared_ptr<const messages::Header> header,
                        std::shared_ptr<const messages::Body> body) {
  LOG_DEBUG << "Got a WORLD message";
  using ps = networking::Peer::Status;
  auto world = static_cast<const messages::World *>(body.get());

  if (world->peers.size() > 0) {
    LOG_DEBUG << "Peers list from remote bot: " << world->peers;
  }

  for (auto &peer : world->peers) {
    auto predicate = [peer, this](const auto &it) {
      return (it->ip() == peer->ip() && it->port() == peer->port()) ||
             (peer->ip() == this->_tcp->local_ip() &&
              peer->port() == this->_tcp->listening_port());
    };
    if (std::find_if(_peers.begin(), _peers.end(), predicate) != _peers.end()) {
      continue;
    }

    _peers.emplace_back(peer);
  }

  auto connection_id = header->connection_id();
  auto rpeer = _tcp->remote_peer(connection_id);

  if (!rpeer) {
    LOG_ERROR << "Got an empty peer. Going out of the world handler";
    return;
  }

  // we should have it in our unconnected list because we told him "hello"
  auto peer_it =
      std::find_if(_peers.begin(), _peers.end(), [&rpeer](const auto &it) {
        return it->ip() == rpeer->ip() && it->port() == rpeer->port();
      });
  std::shared_ptr<networking::Peer> remote_peer;
  if (peer_it == _peers.end()) {
    remote_peer =
        std::make_shared<networking::Peer>(rpeer->ip(), rpeer->port());
  } else {
    remote_peer = *peer_it;
  }

  if (!world->accepted) {
    LOG_DEBUG << "Not accepted, disconnecting ...";
    remote_peer->status(ps::FULL);
    _tcp->disconnected(connection_id, remote_peer);
  } else {
    remote_peer->status(ps::CONNECTED);
    _connected_peers += 1;
  }

  this->status();
  this->keep_max_connections();
}

void Bot::handler_hello(std::shared_ptr<const messages::Header> header,
                        std::shared_ptr<const messages::Body> body) {
  LOG_DEBUG << "Got a HELLO message";
  using ps = networking::Peer::Status;
  auto hello = static_cast<const messages::Hello *>(body.get());
  auto connection_id = header->connection_id();

  // == Create world message for replying ==
  auto message = std::make_shared<messages::Message>();
  bool accepted = _connected_peers < _max_connections;
  networking::Peers peers_tosend;

  if (_connected_peers > 0) {
    for (const auto &peer_conn : _peers) {
      if (peer_conn->status() == ps::CONNECTED ||
          peer_conn->status() == ps::REACHABLE) {
        peers_tosend.emplace_back(peer_conn);
      }
    }
  }

  message->insert(
      std::make_shared<messages::World>(accepted, peers_tosend, header->id()));
  message->header()->transport_layer_id(header->transport_layer_id());
  message->header()->connection_id(connection_id);
  _networking->send_unicast(message);

  // == Check if we need to add this peer to our list or not ==
  auto remote_ip = _tcp->remote_ip(connection_id);
  if (remote_ip == networking::IP{}) {
    LOG_ERROR << "Got an empty IP. Going out of the hello handler";
    return;
  }
  auto peer_it = std::find_if(
      _peers.begin(), _peers.end(), [&hello, &remote_ip](const auto &it) {
        return it->ip() == remote_ip && it->port() == hello->listen_port;
      });
  bool found = peer_it != _peers.end();

  std::shared_ptr<networking::Peer> remote_peer;
  if (!found) {
    remote_peer = std::make_shared<networking::Peer>(
        remote_ip, hello->listen_port, crypto::EccPub(header->sender_key()));
  } else {
    remote_peer = *peer_it;
  }

  if (accepted) {
    remote_peer->status(ps::CONNECTED);
    _connected_peers += 1;
  } else {
    remote_peer->status(ps::REACHABLE);
  }

  if (!found) {
    _peers.emplace_back(remote_peer);
  }
}

bool Bot::init() {
  log::from_config(_conf->logs());

  if (!_conf->key_priv_path().empty() && !_conf->key_pub_path().empty()) {
    _keys = std::make_shared<crypto::Ecc>(_conf->key_priv_path(),
                                          _conf->key_pub_path());
  } else {
    LOG_INFO << "Generating new keys";
    _keys = std::make_shared<crypto::Ecc>();
  }

  const auto &networking_conf = _conf->networking();
  _networking = std::make_shared<networking::Networking>(
      networking_conf.max_connections());

  _max_connections = networking_conf.max_connections();
  _keep_status = networking_conf.keep_status();
  auto tcp_conf = networking_conf.tcp();
  if (tcp_conf) {
    auto parser = messages::Parser::create(tcp_conf->parser());
    _tcp = std::make_shared<networking::Tcp>(_networking.get(), _keys.get(),
                                             parser);

    for (const auto port : tcp_conf->listen_ports()) {
      _tcp->accept(port);
      LOG_INFO << "Accepting connections on port " << port;
    }

    _networking->push(_tcp);
    auto peers_filepath = tcp_conf->peers_filepath();
    if (!peers_filepath.empty()) {
      _peers_conf = std::make_shared<config::Peers>(peers_filepath);
    }

    if (_peers_conf) {
      _peers = _peers_conf->networking_peers();
    } else {
      LOG_WARNING << "There is no information about peers";
    }
  }

  _networking->subscribe(messages::Type::HELLO,
                         [this](std::shared_ptr<const messages::Header> header,
                                std::shared_ptr<const messages::Body> body) {
                           this->handler_hello(header, body);
                         });
  _networking->subscribe(messages::Type::WORLD,
                         [this](std::shared_ptr<const messages::Header> header,
                                std::shared_ptr<const messages::Body> body) {
                           this->handler_world(header, body);
                         });
  _networking->subscribe(messages::Type::CONNECTION,
                         [this](std::shared_ptr<const messages::Header> header,
                                std::shared_ptr<const messages::Body> body) {
                           this->handler_connection(header, body);
                         });
  _networking->subscribe(messages::Type::DECONNECTION,
                         [this](std::shared_ptr<const messages::Header> header,
                                std::shared_ptr<const messages::Body> body) {
                           this->handler_deconnection(header, body);
                         });
  return true;
}

void Bot::update_peers_file(const std::string &filename) const {
  // Writing only the connected peers
  networking::Peers to_write;
  using ps = networking::Peer::Status;
  for (const auto &peer : _peers) {
    if (_keep_status == ps::FULL || peer->status() == _keep_status) {
      to_write.emplace_back(peer);
    }
  }
  _peers_conf->write(to_write);
}

void Bot::status() const {
  LOG_INFO << "Bot: {connected: " << _connected_peers
           << ", peers_list::size: " << _peers.size()
           << ", max_connections: " << _max_connections << "}";
}

bool Bot::next_to_connect(std::shared_ptr<networking::Peer> &peer) {
  using ps = networking::Peer::Status;
  // TODO: Put random or whatever method here to get the next
  for (auto &tmp_peer : _peers) {
    if (tmp_peer->status() == ps::REACHABLE) {
      peer = tmp_peer;
      return true;
    }
  }
  return false;
}

void Bot::keep_max_connections() {
  if (_peers.empty()) {
    LOG_INFO << "Stayng server mode";
    return;
  }

  if (_connected_peers == _max_connections)
    return;

  if (_connected_peers == _peers.size()) {
    LOG_WARNING << "There is no available peer to check";
    return;
  }

  if (_connected_peers < _max_connections) {
    std::shared_ptr<networking::Peer> peer;
    if (this->next_to_connect(peer)) {
      LOG_DEBUG << "Asking to connect to " << *peer;
      peer->status(networking::Peer::Status::CONNECTING);
      _tcp->connect(peer);
    } else {
      LOG_DEBUG << "No more REACHABLE peers";
    }
  }
}

networking::Networking *Bot::networking() { return _networking.get(); }
networking::Tcp *Bot::tcp() { return _tcp.get(); }
void Bot::stop() { _networking->stop(); }
void Bot::join() {
  this->keep_max_connections();
  _networking->join();
}
Bot::~Bot() { stop(); }
} // namespace neuro
