#!/bin/bash -ex
docker build  -f Dockerfile.ubuntu --tag registry.gitlab.com/neurochaintech/bot/ubuntu:18.04  . && docker push registry.gitlab.com/neurochaintech/bot/ubuntu:18.04
docker build  -f Dockerfile.fedora --tag registry.gitlab.com/neurochaintech/bot/fedora:28  . && docker push registry.gitlab.com/neurochaintech/bot/fedora:28
docker build  -f Dockerfile.debian --tag registry.gitlab.com/neurochaintech/bot/debian:sid  . && docker push registry.gitlab.com/neurochaintech/bot/debian:sid
